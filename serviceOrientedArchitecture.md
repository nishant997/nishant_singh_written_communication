# Service-Oriented Architecture

Service-oriented architecture Is an architectural approach in which applications uses the service available in the network, In this application will form through a communication call over the Internet.

- SOA helps users to use facilities from existing services to form applications.
- SOA encompasses a set of design principles that structure system development and provide means for Integrating components Into a coherent and decentralized system.

## There are two major roles within Service-oriented Architecture

1. Service provider : The service provider is the maintainer of service who makes available services to all. To advertise services, the provider can publish them in a registry, together with a service contract, how to use It and the fees charged.

2.Service consumer: The service consumer find out the service metadata in the registry and use the required client components to bind the service.

![Image of service request or response](https://media.geeksforgeeks.org/wp-content/uploads/Screenshot-245.png)

## Components of SOA

![Image for Components of SOA](https://media.geeksforgeeks.org/wp-content/uploads/Screenshot-248.png)

## Guiding Principles of SOA

1. Loose coupling: One service should not depend upon other services
2. Reusability: services should be design in such a way that that can be reuse
3. Discoverability: Services are defined by description documents by which third party easily understand the service and use that

## Advantages of SOA

1. Service reusability: we know in soa the services made from existing services so we can made many application by existing services.

2. Easy maintenance: we know that services are Independent to each other so change in one service will not affect the other service so maintenance is easy.

3. Platform Independent: In SOA new service or application can be made by using older services of SOA which had made on different platform, so SOA is platform Independent

4. Availability: SOA services are easily available.

## Disadvantages of SOA

1. Complex service management: When services interact they exchange messages to tasks. the number of messages may go in millions. It become tedious to handle.

2. High investment: A huge initial investment is required for SOA.

### Helpful links

<https://www.geeksforgeeks.org/service-oriented-architecture/> - automatic!
[GeeksForGeeks](https://www.geeksforgeeks.org/service-oriented-architecture/)
